package com.joanthony.gnbw.file_chooser;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.joanthony.gnbw.DataBaseHelper;
import com.joanthony.gnbw.R;
import android.app.ListActivity;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class FileChooser extends ListActivity {

	private File currentDir;
	private FileArrayAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		currentDir = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath());
		fill(currentDir);
	}

	private void fill(File f) {
		File[] dirs = f.listFiles();
		this.setTitle("Current Path: " + f.getPath());
		List<FileOption> dir = new ArrayList<FileOption>();
		List<FileOption> fls = new ArrayList<FileOption>();
		try {
			for (File ff : dirs) {
				
				String fileName = ff.getName();
				// GET extenstion to make sure that only text files are visible
				String extension = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
				if (ff.isDirectory()) {
					dir.add(new FileOption(fileName, "Folder", ff
							.getAbsolutePath()));
				} else {
					if (extension.equals("txt")){
						fls.add(new FileOption(fileName, "File Size: "
								+ ff.length(), ff.getAbsolutePath()));
					}
				}
			}
		} catch (Exception e) {
			System.out.println("lookie here:" + e);
		}
		Collections.sort(dir);
		Collections.sort(fls);
		dir.addAll(fls);
		if (!f.getName().equalsIgnoreCase(
				Environment.getExternalStorageDirectory().getName())) {
			dir.add(0, new FileOption("..", "Parent Directory", f.getParent()));
		}
		adapter = new FileArrayAdapter(FileChooser.this, R.layout.file_chooser_view,
				dir);
		this.setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		FileOption o = adapter.getItem(position);
		if (o.getData().equalsIgnoreCase("folder")
				|| o.getData().equalsIgnoreCase("parent directory")) {
			currentDir = new File(o.getPath());
			fill(currentDir);
		} else {
			onFileClick(o);
		}
	}

	private void onFileClick(FileOption o) {

		String bookPath = o.getName();
		String extension = bookPath.substring(bookPath.lastIndexOf(".")+1, bookPath.length()); //filenameArray[filenameArray.length-1];
		String bookName = bookPath.substring(0, bookPath.lastIndexOf(".")); // filenameArray[0];
//		System.out.println("extension is: " + extension + " and book name is: " + bookName);
		
		// only text files allowed
		if (extension.equals("txt")) { // || (extension.equals("epub"))
			// open db and update file name and file path
			try {
				DataBaseHelper tempDbHelper;
				tempDbHelper = new DataBaseHelper(this, false);
				
				tempDbHelper.addBook(bookName, o.getPath());
				
				tempDbHelper.close();
				tempDbHelper = null;
				
			} catch (Exception e) {
			} finally {
				Toast.makeText(this, "Book added to library",
					Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(this, "Not a text file!",
					Toast.LENGTH_SHORT).show();
		}
		
		// if you want to get the full path, use o.getPath() instead
	}

	@Override
	public void onBackPressed() {
		this.finish();
	}
}
