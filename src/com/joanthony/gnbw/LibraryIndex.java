/**
 * 
 */
package com.joanthony.gnbw;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * This class should show list of books in the database when instantiated.
 * It uses a loader in order to query the database
 * @author Tony
 * http://developer.android.com/guide/topics/ui/layout/listview.html
 */
public class LibraryIndex extends ListActivity implements OnItemLongClickListener {
	
	// String array to store the book names appearing in the library
	String[] listItems;
	
	// variable to store the paths of each book from 0 to n
	String[] bookPath;
	
	/* 
	 * On create of this view we want to pull the list of books from the DB and then set it to the adapter
	 * and then show the adapter
	 * @param Bundle
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//creates the index
		setListItems();

		setContentView(R.layout.activity_view_list_view);	
		
		// setting the long click event to pull up the context menu
		registerForContextMenu(this.getListView());
		getListView().setOnItemLongClickListener(this);
	}

	/**
	 * Method to get list of books from the db and show in the libindex
	 */
	private void setListItems() {
		DataBaseHelper myDbHelper = new DataBaseHelper(this, true);
		
		String[] myArray = myDbHelper.getLibraryIndex();

		ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(this, R.layout.menu_list_item, myArray);
		setListAdapter(myAdapter);
		myDbHelper.close();
		myDbHelper = null;
	}	
	
	/* (non-Javadoc)
	 * @see android.app.ListActivity#onListItemClick(android.widget.ListView, android.view.View, int, long)
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		
		// cast id to int; selected book id is 0, in the DB it is 1
		int selectedBookID = (int) id;
				
		DataBaseHelper myDbHelper = new DataBaseHelper(this, false);
	
		myDbHelper.openBook(selectedBookID);
		
		myDbHelper.close();
		myDbHelper = null;
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		return false;
	}
	
	/* (non-Javadoc)
	 * creation of the contextmenu
	 * @see android.support.v4.app.Fragment#onCreateContextMenu(android.view.ContextMenu, android.view.View, android.view.ContextMenu.ContextMenuInfo)
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		
		super.onCreateContextMenu(menu, v, menuInfo);
		
	    menu.add(0, 0, 0, R.string.button_delete_book);
	    
	}
	
	/* (non-Javadoc)
	 * code to run when item in context menu is selected - version 1 will only have delete option
	 * @see android.support.v4.app.Fragment#onContextItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onContextItemSelected(MenuItem item) {		
		
		DataBaseHelper myDbHelper = new DataBaseHelper(this, false);
		
		// code to delete book
		if (item.getItemId() == 0){
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		
			// info.id is the index of the selected menu item
			//don't delete book 1 and 2 which are preloaded. same as info.id 0 and 1
			if ((int) info.id <=1){
				Toast.makeText(this, "Cannot delete preloaded books, Sorry", Toast.LENGTH_LONG).show();
			} else {
				//delete book from DB; info.id is 1; in DB it is 1
				myDbHelper.deleteBook((int) info.id);
				Toast.makeText(this, "Book Deleted", Toast.LENGTH_LONG).show();
				
				// refresh library view
				setListItems();
			}
		} else if (item.getItemId() == 1){ //code for reset game
			//dummy code for later use if and when we add code for reset of game from lib
		}

		myDbHelper.close();
		myDbHelper = null;
		
		return super.onContextItemSelected(item);
	}

}
