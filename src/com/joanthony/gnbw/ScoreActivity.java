package com.joanthony.gnbw;

import com.joanthony.gnbw.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ScoreActivity extends Activity {

	public static final String STRING_TOTAL = "total";
	public static final String STRING_COUNT = "count";
	public static final String STRING_ENTRY = "entry";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.score_view);

		LinearLayout globalLayout = (LinearLayout) this
				.findViewById(R.id.score_global_layout);

		globalLayout.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// Some devices dont have a menu button
				openOptionsMenu();
				return true;
			}
		});

		// get extra data
		int entryCount = 1;
		while (true) {
			// get title
			String title = this.getIntent().getStringExtra(
					STRING_ENTRY + entryCount);
			// get correct answer count
			int count = this.getIntent().getIntExtra(STRING_COUNT + entryCount,
					-1);
			// get total error
			int total = this.getIntent().getIntExtra(STRING_TOTAL + entryCount,
					-1);

			// if all of the above values are successfully retrieved, add a new
			// entry
			if (title != null && count > -1 && total > -1 && count <= total) {
				// wrapper
				LinearLayout wrappy = new LinearLayout(this);
				wrappy.setOrientation(LinearLayout.VERTICAL);
				
				// title
				TextView textViewTitle = new TextView(this);
				textViewTitle.setText(title);
				textViewTitle.setTextAppearance(this,
						android.R.style.TextAppearance_Large);
				wrappy.addView(textViewTitle);
				
				// charge bar
				ProgressBar progress = new ProgressBar(this, null,
						android.R.attr.progressBarStyleHorizontal);
				progress.setMax(total);
				progress.setProgress(count);
				wrappy.addView(progress);
				
				// initializing a final variable because otherwise we can't use a non-final variable inside an anonymous inner class
				final String clickedTitle = title;
				wrappy.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// Show articles relevant to error type clicked
						showArticle(clickedTitle);
					}
				});
				globalLayout.addView(wrappy);
				
				
				entryCount++;
			} else {
				break;
			}
		}
	}

	
	/**
	 * This function will show a text view with articles relevant to the error type
	 * @param errorType As chosen by click of user in score view
	 */
	protected void showArticle(String errorType) {

		// initializing a final variable because otherwise we can't use a non-final variable inside an anonymous inner class
		final String type = errorType;
		
        //Ask the user if they want to open browser
		new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle(R.string.confirm)
        .setMessage(R.string.really_confirm)
        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {


            @Override
            public void onClick(DialogInterface dialog, int which) {
        	
            	if (type.equals("Articles")){
        			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://owl.english.purdue.edu/owl/resource/540/01/"));
        			startActivity(browserIntent);
        		} else if (type.equals("Pronouns")){
        			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://owl.english.purdue.edu/owl/resource/595/1/"));
        			startActivity(browserIntent);
        		} else if (type.equals("Definitional")){
        			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://owl.english.purdue.edu/owl/resource/660/01/"));
        			startActivity(browserIntent);
        		} else if (type.equals("Preposition")){
        			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://owl.english.purdue.edu/owl/resource/594/1/"));
        			startActivity(browserIntent);
        		} else if (type.equals("Tenses")){
        			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://owl.english.purdue.edu/owl/resource/601/1/"));
        			startActivity(browserIntent);
        		}	
        		
            }

        })
        .setNegativeButton(R.string.no, null)
        .show();


	}

	public static void addEntry(Context context, Intent intent, int index,
			String title, int progress, int total) {
		intent.putExtra(STRING_ENTRY + index, title);
		intent.putExtra(STRING_COUNT + index, progress);
		intent.putExtra(STRING_TOTAL + index, total);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.score, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// reset score
		if (item.getItemId() == R.id.btn_ResetScore) {

			DataBaseHelper myDbHelper = new DataBaseHelper(this, true);

			myDbHelper.resetScore();

			myDbHelper.close();
			myDbHelper = null;
			// refresh score view by just exiting the score view... for the time
			// being
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

}
