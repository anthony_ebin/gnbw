package com.joanthony.gnbw;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import com.joanthony.gnbw.R;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.view.MotionEvent;

public class PageViewer extends Activity {

	ArrayList<HashMap<String, String>> arrayedTxtBuff;
	int countOfGameErrors;
	int countOfArticlesErrors;
	int countOfPronounsErrors;
	int countOfDefinitionalErrors;
	int countOfPrepositionErrors;
	int countOfTensesErrors;
	int currentArticlesScore;
	int currentPronounsScore;
	int currentDefinitionalScore;
	int currentPrepositionScore;
	int currentTensesScore;
	int currentGameScore;
	int MAX_LINES_PER_LEVEL; // = 5;
	int FONT_SIZE;
	int lastPosition;
	String bookPath;
	int thisBookID;
	int nextPosition;
	int lineNumber;

	// page turn
	private OnSwipe mSwipeHandler;
	private boolean isInViewingState;
	// page turn variables
	private int pThisBookID;
	private String pBookPath;
	private int pActualPosition;
	private int pNumLinesPerPage;
	private int pFontSize;
	private String pTxtBuff;
	private int pStartPosition;
	private String pSwipeType;
	private int pActualNextPosition;
	private int pThisNextPosition;
	private int pLineNumber;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		// importing preferences
		SharedPreferences getPrefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		MAX_LINES_PER_LEVEL = Integer.parseInt(getPrefs.getString("MAX_LINES",
				"10"));
		FONT_SIZE = Integer.parseInt(getPrefs.getString("FONT", "35"));
		
		if (getPrefs.getBoolean("DIM_OFF", true) == true){
			// keep screen on
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		Bundle extras = getIntent().getExtras();

		// get bookPath, bookID and lastPosition from library
		bookPath = extras.getString("BookPath");
		thisBookID = extras.getInt("BookID");
		lastPosition = extras.getInt("LastPosition");
		
		setTitle(extras.getString("BookName"));

		setContentView(R.layout.activity_main);
		
		arrayedTxtBuff = getPageContents();// txtBuff.split("\\s");
		
		if (arrayedTxtBuff == null){
			this.finish();
			return;
		}

		setVariationsInArray();

		// submit button
		final ImageButton submitButton = (ImageButton) findViewById(R.id.next_level);

		// game text view
		final Page gameText = (Page) findViewById(R.id.game_text);

		// using special edittext to display level
		gameText.setData(arrayedTxtBuff);
		// setting font size
		gameText.setTextSize(FONT_SIZE);
		// this.findViewById(R.id.game_text).setScrollbarFadingEnable)

		// setting swipe event
		mSwipeHandler = new OnSwipe(this) {
			public void onSwipeRight() {
				if (!isInViewingState) {
					// code to show previous pages only if currently on page 1
					// or
					// after
					System.out.println("lastPosition:" + lastPosition);
					if (lastPosition > 0) {
						pBookPath = bookPath;
						pThisBookID = thisBookID;
						pActualPosition = lastPosition;
						pActualNextPosition = nextPosition;
						pNumLinesPerPage = MAX_LINES_PER_LEVEL;
						pFontSize = FONT_SIZE;
						pSwipeType = "right";

						pStartPosition = pActualPosition - pNumLinesPerPage;

						// switch to viewing state
						pTxtBuff = pLoadBookViewer();
						gameText.setTextSize(pFontSize);
						gameText.setText(pTxtBuff);
						gameText.setClickable(false);
						pLoadPageNum();

						isInViewingState = true;
						submitButton.setEnabled(false);
					}
				} else {
					// code to show previous pages only if currently on page
					// 1 or after
					if (pSwipeType.equals("right")) {
						if (pStartPosition > 0) {
							pStartPosition = pStartPosition - pNumLinesPerPage;
							pTxtBuff = pLoadBookViewer();
							gameText.setText(pTxtBuff);
							pLoadPageNum();
						}
					} else if (pSwipeType.equals("left")) {
						// switch back to game state
						isInViewingState = false;
						gameText.setDisplayType(gameText.getDisplayType());
						gameText.setTextSize(FONT_SIZE);
						gameText.setClickable(true);
						loadPageNum();
						submitButton.setEnabled(true);
					}
				}
			}

			public void onSwipeLeft() {
				if (!isInViewingState) {
					// code to show next pages only if currently on last page or
					// before
					pBookPath = bookPath;
					pThisBookID = thisBookID;
					pActualPosition = lastPosition;
					pActualNextPosition = nextPosition;
					pNumLinesPerPage = MAX_LINES_PER_LEVEL;
					pFontSize = FONT_SIZE;
					pSwipeType = "left";

					pStartPosition = pActualNextPosition;

					// switch to viewing state
					pTxtBuff = pLoadBookViewer();
					gameText.setTextSize(pFontSize);
					gameText.setText(pTxtBuff);
					gameText.setClickable(false);

					isInViewingState = true;
					submitButton.setEnabled(false);
				} else {
					// code to show next pages only if currently on last
					// page or before
					// also only run code only for swipetype = right because
					// if moving book forward then we show only one line
					if (pSwipeType.equals("right")) {
						if (pStartPosition < pActualPosition) {
							pStartPosition = pStartPosition + pNumLinesPerPage;
							if (pStartPosition == pActualPosition) {
								// switch back to game state
								isInViewingState = false;
								gameText.setDisplayType(gameText
										.getDisplayType());
								gameText.setTextSize(FONT_SIZE);
								gameText.setClickable(true);
								loadPageNum();
								submitButton.setEnabled(true);
							} else {
								pTxtBuff = pLoadBookViewer();
								gameText.setText(pTxtBuff);
								pLoadPageNum();
							}
						}
					}
				}
			}

			public boolean onTouch(View v, MotionEvent event) {
				gestureDetector.onTouchEvent(event);
				return false;
			}
		};

		loadPageNum();

		// page turn
		isInViewingState = false;
}

	/**
	 * @param GrammErrorsArray
	 */
	private void setVariationsInArray() {
		
		DataBaseHelper myDBHelper = new DataBaseHelper(this, true);
		// SYSTEM pulls table GrammErrors from DB and stores in array called
		// GrammErrorsArray
		String[][] GrammErrorsArray = myDBHelper.getGrammErrors();
		
		myDBHelper.close();
		myDBHelper=null;
		
		// For each word in grammerrorsarray check if current word is found. If found then store variations
		// be careful: GrammErrorsArray.length is 9(number of columns) whereas
		// GrammErrorsArray[0].length-1 is 94 (this is what we need, number of
		// rows -1)
		for (int x = 0; x <= GrammErrorsArray[0].length - 1; x++) {

			for (HashMap<String, String> map : arrayedTxtBuff) {
				
				// we don't want to accidentally pick up whitespaces
				String currentWord = map.get("originalWord").trim();
				
				// only run the code on non blanks
				if (currentWord.length() >= 1) {
					// variation1 to variation6 is column 3 to column 8
					for (int variation = 3; variation < 9; variation++) {
						// GrammErrorsArray[2] is wordVariations column
						if ((GrammErrorsArray[variation][x]
								.equals(currentWord))) {

							map.put("typeOfError",
									GrammErrorsArray[1][x]);
//							map.put("tip", GrammErrorsArray[2][x]);
							map.put("variation1",
									GrammErrorsArray[3][x]);
							map.put("variation2",
									GrammErrorsArray[4][x]);
							map.put("variation3",
									GrammErrorsArray[5][x]);
							map.put("variation4",
									GrammErrorsArray[6][x]);
							map.put("variation5",
									GrammErrorsArray[7][x]);
							map.put("variation6",
									GrammErrorsArray[8][x]);

							// choosing errors for this level
							// int idx = new Random().nextInt(High-Low) + Low;
							// 5 is the number of hardcoded variations but I don't get an 8
							// unless i put a range of 6
							int idx = new Random().nextInt(6) + 1;
							String randVariation = map.get("variation" + idx);

							if (randVariation.length() > 0) {
								
								//this column allows easy filtering of errored words during highlighting
								map.put("errored", "Yes");
								//increment total count of errors regardless
								countOfGameErrors++;
								map.put("errorWord", randVariation);
								
								String errorType = map.get("typeOfError");
								// adding to total count of types of errors
								if (errorType.equals("Articles")) {
									countOfArticlesErrors++;
								} else if (errorType.equals("Pronouns")) {
									countOfPronounsErrors++;
								} else if (errorType.equals("Definitional")) {
									countOfDefinitionalErrors++;
								} else if (errorType.equals("Preposition")) {
									countOfPrepositionErrors++;
								} else if (errorType.equals("Tenses")) {
									countOfTensesErrors++;
								}
							}

							// break out of for loop if word is found
							break;
						}
					}
				}
			}	
		}
	}
	
	/**
	 * This function opens the book, reads the relevant page and returns an array list. the first column of 
	 * the array list is the page split by 1 word per row. the next few columns are place holders for currentGameErrorArray 
	 * information to be loaded later
	 * @return returns an array list
	 */
	private ArrayList<HashMap<String, String>> getPageContents() {
		
		InputStream inputStream;

		if (thisBookID == 1) {
			inputStream = this.getResources().openRawResource(
					R.raw.metamorphosis);
		} else if (thisBookID == 2) {
			inputStream = this.getResources().openRawResource(
					R.raw.the_lost_world);
		} else {
			File f_path = new File(bookPath);
			// will fill with imported books later
			inputStream = null;
			try {
				inputStream = new BufferedInputStream(new FileInputStream(
						f_path));
			} catch (FileNotFoundException e) {
				Toast.makeText(this,
				"No file found; may have been deleted or moved!!",
				Toast.LENGTH_SHORT).show();
				e.printStackTrace(); 
				return null;
			}
		}
		InputStreamReader inputreader = new InputStreamReader(inputStream);
		// the buffered reader has a default size of 8192
		BufferedReader buffreader = new BufferedReader(inputreader, 2500);
		String line;
		StringBuilder textBuffer = new StringBuilder();

		//setting next page position
		nextPosition = lastPosition + MAX_LINES_PER_LEVEL;
		try {
			// vary number of lines from settings
			lineNumber = 0;
			// don't worry about running the buffered reader until end of file: http://stackoverflow.com/questions/1277880/how-can-i-get-the-count-of-line-in-a-file-in-an-efficient-way
			while ((line = buffreader.readLine()) != null) {
				if ((lineNumber >= lastPosition)&& (lineNumber < nextPosition)) {
					textBuffer.append(line);
					textBuffer.append('\n');
				}
				lineNumber++;
			}
		} catch (IOException e) {
			// system.out.println(e);
		}
		inputStream = null;
		buffreader = null;
		
		String[] txtArray = textBuffer.toString().split("\\s");
		
		ArrayList<HashMap<String, String>> pageArrayList = new ArrayList<HashMap<String, String>>();
		
		// add each word to the map; also add placeholders
		for (int i = 0; i < txtArray.length; i++) {
			// instatiate a new hashmap for each word
			HashMap<String, String> pageAndGameDataMap = new HashMap<String, String>();
			
			pageAndGameDataMap.put("originalWord",
					txtArray[i]);
			//error word column is a duplicate of originalWord except that certain words would be switched around
			pageAndGameDataMap.put("errorWord",
					txtArray[i]);
			pageAndGameDataMap.put("errored",
					"");
			pageAndGameDataMap.put("typeOfError",
					"");
			pageAndGameDataMap.put("variation1",
					"");
			pageAndGameDataMap.put("variation2",
					"");
			pageAndGameDataMap.put("variation3",
					"");
			pageAndGameDataMap.put("variation4",
					"");
			pageAndGameDataMap.put("variation5",
					"");
			pageAndGameDataMap.put("variation6",
					"");
			
			pageArrayList.add(pageAndGameDataMap);
		}
		
		return pageArrayList;
		
	}

	/* (non-Javadoc)
	 * if game is in viewing state; i.e. the player is looking at previous or next pages then go back to game
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (isInViewingState==true){
			// switch back to game state
			isInViewingState = false;
			
			// submit button
			final ImageButton submitButton = (ImageButton) findViewById(R.id.next_level);

			// game text view
			final Page gameText = (Page) findViewById(R.id.game_text);
			
			gameText.setDisplayType(gameText.getDisplayType());
			gameText.setTextSize(FONT_SIZE);
			gameText.setClickable(true);
			loadPageNum();
			submitButton.setEnabled(true);
			
		} else super.onBackPressed();
	}

	private void loadPageNum() {
		// show page- first convert line number to pagenumber by dividing by
		// max_lines
		TextView pageNum = (TextView) this.findViewById(R.id.page_number);
		pageNum.setText("Page "+ lastPosition / MAX_LINES_PER_LEVEL + " of " + lineNumber
				/ MAX_LINES_PER_LEVEL);
	}


	/**
	 * method that runs on click of submit button, counts score, submits to DB and shows score/answers on screen
	 * @param view
	 */
	public void submitAnswers(View view) {

		ImageButton btnSubmit = (ImageButton) this.findViewById(R.id.next_level);

		// if in game view then count score and show score view; if in score
		// view then allow code for next level
		Page e = (Page) this.findViewById(R.id.game_text);
		
		//the page instance has the latest arrayedTxtBuff with the chosen word and stuff!! So we need to reload the arrayedTxtBuff
		this.arrayedTxtBuff = e.arrayedTxtBuff;
		// if in game mode (clickable) as opposed to score mode
		if (e.getDisplayType().equals(Page.DisplayType.Clickable)) { //was if (e.isClickable()) {

			countScore();

			// SYSTEM add score to OverallProgress table in DB
			
			DataBaseHelper myDbHelper = new DataBaseHelper(this, false);
			myDbHelper.updateOverallProgress(currentArticlesScore,
					countOfArticlesErrors, currentPronounsScore,
					countOfPronounsErrors, currentDefinitionalScore,
					countOfDefinitionalErrors, currentPrepositionScore,
					countOfPrepositionErrors, currentTensesScore,
					countOfTensesErrors);
			// don't update next position at the end of the book
			if (nextPosition <= lineNumber){
				myDbHelper.updateNextPosition(nextPosition, thisBookID);
			}

			myDbHelper.close();
			myDbHelper=null;
			
			// show score
			EditText gameScoreText = (EditText) this
					.findViewById(R.id.game_score_text);
			gameScoreText.setText(currentGameScore + " / " + countOfGameErrors);

			// make the page instance unclickable so that we can show score
			e.setClickable(false);
			e.setDisplayType(Page.DisplayType.Score);
			
		    // setting image to next
			btnSubmit.setImageResource(R.drawable.ic_next);
			
		} else {
			
			// go to next level if the book is not finished
			if (nextPosition <= lineNumber) {
				// btnSubmit.setText(R.string.button_submit);
				Intent openMainReader = new Intent(this, PageViewer.class);
	
				openMainReader.putExtra("BookPath", bookPath);
				openMainReader.putExtra("BookID", thisBookID);
				openMainReader.putExtra("LastPosition", nextPosition);
	
				// openMainReader.putExtra("LastPosition", lastPosition);
				this.startActivity(openMainReader);
				this.finish();
			} else {
				Toast.makeText(this, "Book complete!! Try another :)", Toast.LENGTH_SHORT).show();
				this.finish();
			}
		}

	}

	/**
	 * function to add score to class fields
	 */
	private void countScore() {
		currentGameScore = 0;
		currentArticlesScore = 0;
		currentPronounsScore = 0;
		currentDefinitionalScore = 0;
		currentPrepositionScore = 0;
		currentTensesScore = 0;

		for (HashMap<String, String> map : arrayedTxtBuff) {



			if (map.get("errored") == "Yes"){
				//save calculation time by initializing here instead of outside this
				String chosenWord = map.get("errorWord");
				String actualWord = map.get("originalWord");
				String typeOfError = map.get("typeOfError");
				if (chosenWord.equals(actualWord)) {
					currentGameScore++;
					if (typeOfError.equals("Articles")) {
						currentArticlesScore++;
					} else if (typeOfError.equals("Pronouns")) {
						currentPronounsScore++;
					} else if (typeOfError.equals("Definitional")) {
						currentDefinitionalScore++;
					} else if (typeOfError.equals("Preposition")) {
						currentPrepositionScore++;
					} else if (typeOfError.equals("Tenses")) {
						currentTensesScore++;
					}
				}
			}
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		super.dispatchTouchEvent(ev);
		return mSwipeHandler.onTouch(null, ev);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// reset game
		if (item.getItemId() == R.id.btn_ResetGame) {
			// pass view and lastPosition = 0
			Intent openMainReader = new Intent(this, PageViewer.class);

			openMainReader.putExtra("BookPath", bookPath);
			openMainReader.putExtra("BookID", thisBookID);
			openMainReader.putExtra("LastPosition", 0);

			// openMainReader.putExtra("LastPosition", lastPosition);
			this.startActivity(openMainReader);
			this.finish();
		} else if (item.getItemId() == R.id.btn_SkipPages) { //skip pages
			
			//get number of pages from user
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			
			alert.setTitle("Skip");
			alert.setMessage("How many pages to skip?");
			
			// Set an EditText view to get user input 
			final EditText input = new EditText(this);
			
			// validation - only allow whole numbers
			input.setInputType(InputType.TYPE_CLASS_NUMBER);
			
			alert.setView(input);

			alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				
				
				public void onClick(DialogInterface dialog, int whichButton) {
	
					final int pagesToSkip = Integer.parseInt(input.getText().toString());
					
					// if pagesToSkip = 5, total pages = 10, last position=2; max = 10-2 = 8; return true for "if < max"
					int maxPagesToSkip = (lineNumber/MAX_LINES_PER_LEVEL) - (lastPosition / MAX_LINES_PER_LEVEL);
				  
					  if (pagesToSkip>0){
						  //<= because: e.g. we are on page 9 out of 10. max skipable = 10-9=1. if rule
						  // is skip if < max then the last page will not skip because 1 < 1 = false
						if (pagesToSkip<=maxPagesToSkip){
							  skipPages(pagesToSkip);
						  }else { // more than max number of pages to skip
							  Toast.makeText(PageViewer.this, "You cannot skip past the last page", Toast.LENGTH_SHORT).show();
						  }
							  
					  }
				}
			});

			alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			  public void onClick(DialogInterface dialog, int whichButton) {
			    // Canceled.
			  }
			});

			alert.show();

		}

		return super.onOptionsItemSelected(item);
	}

	
	/**
	 * @param numOfPagesToSkip number of pages to skip. Max number of pages to skip is limited by total 
	 * number of pages - current page number
	 */
	private void skipPages(int numOfPagesToSkip) {
		System.out.println("skip "+numOfPagesToSkip+" pages");
		// btnSubmit.setText(R.string.button_submit);
		Intent openMainReader = new Intent(this, PageViewer.class);

		// if current position is 10, user asks to skip 5 pages, then skipposition = 10+50 = line number 60
		int skipPosition = lastPosition + (numOfPagesToSkip*MAX_LINES_PER_LEVEL);
		
		openMainReader.putExtra("BookPath", bookPath);
		openMainReader.putExtra("BookID", thisBookID);
		openMainReader.putExtra("LastPosition", skipPosition);
		
		// need to set database last position so that on reopen it doesnt go back to previous level
		DataBaseHelper myDbHelper = new DataBaseHelper(this, false);
		myDbHelper.updateNextPosition(skipPosition, thisBookID);

		myDbHelper.close();
		myDbHelper=null;

		// openMainReader.putExtra("LastPosition", lastPosition);
		this.startActivity(openMainReader);
		this.finish();
		

	}

	/*
	 * ======================================================================
	 * ================================ Page turn ===========================
	 * ======================================================================
	 */
	private void pLoadPageNum() {
		// show page- first convert line number to pagenumber by dividing by
		// max_lines
		TextView pageNum = (TextView) this.findViewById(R.id.page_number);
		pageNum.setText((pStartPosition / pNumLinesPerPage) + " / " + (pLineNumber
				/ pNumLinesPerPage));
	}

	private String pLoadBookViewer() {

		InputStream inputStream;

		if (pThisBookID == 1) {
			inputStream = this.getResources().openRawResource(
					R.raw.metamorphosis);
		} else if (pThisBookID == 2) {
			inputStream = this.getResources().openRawResource(
					R.raw.the_lost_world);
		} else {
			File f_path = new File(pBookPath);
			// will fill with imported books later
			inputStream = null;
			try {
				inputStream = new BufferedInputStream(new FileInputStream(
						f_path));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		InputStreamReader inputreader = new InputStreamReader(inputStream);
		// the buffered reader should have a custom size of 8192 else it wont
		// buffer
		BufferedReader buffreader = new BufferedReader(inputreader, 8192);
		String line;
		StringBuilder textBuffer = new StringBuilder();

		pThisNextPosition = pStartPosition + pNumLinesPerPage;
		System.out.println("thisNextPosition:" + pThisNextPosition);
		try {
			// vary number of lines from settings
			pLineNumber = 0;
			while (((line = buffreader.readLine()) != null)) {
				if ((pLineNumber >= pStartPosition)
						&& (pLineNumber < pThisNextPosition)) {
					textBuffer.append(line);
					textBuffer.append('\n');
					// if moving book forward then show only one line
					if (pSwipeType.equals("left")) {
						break;
					}
				}
				pLineNumber++;
			}
		} catch (IOException e) {
			// system.out.println(e);
		}
		inputStream = null;
		buffreader = null;
		return textBuffer.toString();
	}
}
