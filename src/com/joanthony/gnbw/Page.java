package com.joanthony.gnbw;

import java.util.ArrayList;
import java.util.HashMap;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

public class Page extends EditText {

	public enum DisplayType {
		Clickable, Score
	}

	private DisplayType m_type;
	
	private OnTouchListener m_onTouchListener;
	public ArrayList<HashMap<String, String>> arrayedTxtBuff;
	int idx = 1;

	public Page(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		CustomInit(context);
	}
	public Page(Context context, AttributeSet attrs) {
		super(context, attrs);
		CustomInit(context);
	}
	public Page(Context context) {
		super(context);
		CustomInit(context);
	}
	
	public void setData(ArrayList<HashMap<String, String>> arrayedTxtBuff) {
		this.arrayedTxtBuff = arrayedTxtBuff;
		this.setText(buildString(m_type));
	}
	
	public void setDisplayType(DisplayType type) {
			this.m_type = type;
			this.setText(this.buildString(m_type));
	}

	public DisplayType getDisplayType() {
		return m_type;
	}
	/**
	 * Build a string with clickable word. Use current arrayedTxtBuff and currentGameErrorsArray as input.
	 * @return a SpannableStringBuilder object that can be displayed by EditText
	 */
	private SpannableStringBuilder buildString(DisplayType type) {
		SpannableStringBuilder spannableText = new SpannableStringBuilder();
		int start = 0;
		for (HashMap<String, String> map : arrayedTxtBuff) {
			String originalWord = map.get("originalWord");
			String erroredWord = map.get("errorWord");
			int erroredWordLen = erroredWord.length();
			// if word has been switched we want to identify it through the errored variable
			boolean errored;
			errored = map.get("errored").equals("Yes")? true : false;
			if (erroredWord != null) {
				spannableText.append(erroredWord);
				// int index = findArray(this.currentGameErrorsArray, i);
				// if word has been switched; regardless of whether it is different from original word errored is = true
				if (errored) {
					if (type == DisplayType.Clickable) {
						spannableText.setSpan(new ForegroundColorSpan(Color.BLUE),
								start, start + erroredWordLen,
								Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						spannableText.setSpan(new UnderlineSpan(), start, start
								+ erroredWordLen,
								Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					} else if (type == DisplayType.Score) {
						// display the string as a scored text
						// green for correct words, strike through wrong words
						// String origin = this.currentGameErrorsArray.get(index)
						// .get("originalWord");
						if (originalWord.equals(erroredWord)) {
							// correct word
							spannableText.setSpan(new ForegroundColorSpan(
									Color.GREEN), start, start
									+ erroredWordLen,
									Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						} else {
							// wrong word
							spannableText.setSpan(new StrikethroughSpan(),
									start,
									start + erroredWordLen,
									Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
							spannableText.setSpan(new ForegroundColorSpan(
									Color.RED), start, start
									+ erroredWordLen,
									Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
							// place the original word next to it
							spannableText.append(originalWord);
							spannableText.setSpan(new ForegroundColorSpan(
									Color.BLUE),
									start + erroredWordLen,
									start + erroredWordLen
											+ originalWord.length(),
									Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
							start += originalWord.length();
						}
					}
				}
				spannableText.append(" ");
				//+1 for the whitespace appended above
				start += erroredWordLen + 1;
			}
		}
		if (spannableText.length() > 0) {
			spannableText.delete(spannableText.length() - 1,
					spannableText.length() - 1);
		}
		return spannableText;
	}


	/**
	 * Return index of the word being touched
	 * 
	 * @param text
	 *            contains words
	 * @param blink_index
	 *            index of the blink cursor when text being touched
	 * @return -1 if blink index is out of text range
	 */
	private int wordAt(String text, int blink_index) {

		String[] words = text.split(" ");

		int globalIndex = 0;
		for (int i = 0; i < words.length; i++) {
			// blink index is the position of the character which is being clicked
			if (globalIndex <= blink_index
					&& blink_index <= globalIndex + words[i].length()) {
				// Hello bob master
				// 012345678901234
				// 0-4 5-3 9-6
				return i;
			} else {
				globalIndex += words[i].length() + 1;
			}
		}

		return -1;
	}

	private void CustomInit(Context context) {
		
		//making sure user can't type in words
		this.setFocusable(false);
		this.setClickable(true);
		//setting font
		this.setTypeface(Typeface.SERIF);
		
		// default display style
		this.m_type = DisplayType.Clickable;
		
		// init touch event listener
		// final EditText m_editText = this;
		m_onTouchListener = new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// Phuoc's code; I guess he is saying, if clicked on unclickable item, return true; do nothing
				if(!isClickable()) {
					return true;
				}
				
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					EditText m_editText = (EditText) v;

					// handle touch event
					Layout layout = m_editText.getLayout();
					// ae - i think if i add scrollY here the program should look at both row and column of touch
//					float clickedX = event.getX();
//					float clickedScrolledX = m_editText.getScrollX();
//					float x = clickedX  + clickedScrolledX ; 
					int x = (int)event.getX() + (int)m_editText.getScrollX();
					int y = (int)event.getY() + (int)m_editText.getScrollY();; //+ (int)m_editText.getScrollY();
					
					int line = layout.getLineForVertical(y);
					//offset should represent actual character position of where the user has clicked
					int offset = layout.getOffsetForHorizontal(line, x);
					if (offset > 0)
						if (x > layout.getLineMax(0)) {
							// m_editText.setSelection(offset); // touch was at
							// end
							// of text
						} else {
							offset--;
							// m_editText.setSelection(offset);
						}

					/* perform text-change */
					try {
						// this is the text currently being displayed on
						// EditText
						String currentText = m_editText.getText().toString();
						// this is index of the word being touched
						int wordIndex = wordAt(currentText, offset);
						// this is the word being touched
						String currentWord = currentText.split("\\s")[wordIndex];
						// this is the corresponding word-list of the current
						// touched word
						
						if (arrayedTxtBuff.get(wordIndex).get("errored").equals("Yes")){
							HashMap<String, String> currentWordList = arrayedTxtBuff.get(wordIndex);
//							for (int i = 1; i < 7; i++) {
								// replace the word at wordIndex of the string
								// currentText with the next word in the
								// corresponding list
								// if the clicked word is the same one found in currentWordList variation then go to next variation
								String newWord = cycleNextWord(currentWordList, currentWord);
								//System.out.println("after return: "+newWord);
								arrayedTxtBuff.get(wordIndex).put("errorWord", newWord);

								m_editText.setText(buildString(m_type));
						} 

							
						} catch (Exception e) {
							// some error occur, do nothing
							Log.d("OnTouch error caught", "" + e.getMessage());
					}

					break;
				default:
					break;
				}

				return true;
			}
		};
		this.setOnTouchListener(m_onTouchListener);
	}
	
	// keep cycling through words until you find a non-null word
	private String cycleNextWord(HashMap<String, String> wordList, String currentWord){

		do{
			
			if (idx>6){idx=1;}
			String word = wordList.get("variation"+idx);
			if ((word.length()>=1) && (!word.equals(currentWord))){
				idx++;
				return word;
			}else {
				idx++;
			}
		}while (idx<=6);
		
		return cycleNextWord(wordList, currentWord);
			
	}
}
