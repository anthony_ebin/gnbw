package com.joanthony.gnbw;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Helper class to do all the back end work and return real time just in time info
 * @author Sweety
 *
 */
public class DataBaseHelper extends SQLiteOpenHelper {
	
	// The Android's default system path of the database called GNBW
	// The actual path will be assigned when the program learns the context ( we don't want to hard code the DB path)
	// these variables are static because it doesn't change from instance to instance
    private static final String DB_NAME = "GNBW";
    private static String DB_PATH;
    private static final int DB_VERSION = 1;
    
    // object variable to hold the database
    private SQLiteDatabase myDataBase;
    
    // myContext will hold the current context
    // it's final because it will not be reassigned
    private final Context myContext;

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     * @param context
     * @param isReadOnly true if requesting readonly database, false if requesting writable database
     */
	public DataBaseHelper(Context context, Boolean isReadOnly) 
	{
		super(context, DB_NAME, null, DB_VERSION);
		
		this.myContext = context;
		
		// assigning the DB path contextually during runtime
		DB_PATH =  myContext.getDatabasePath(DB_NAME).toString();	
		
		try {
			createDataBase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			if (isReadOnly){
				this.myDataBase = getReadableDatabase();
			} else{
				this.myDataBase = getWritableDatabase();
			}
		} catch (SQLiteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}

	/**
     * Method that creates an empty database on the system and rewrites it with your own database. 
     * Unless the DB already exists
     **/
    public void createDataBase() throws IOException{
 
    	boolean dbExist = checkDataBase();
    	System.out.println("dbExist"+dbExist);
    	if(dbExist)
    	{
    		// do nothing - database already exist. if this is not done then the program will reset the
    		// database to the initial DB stored in the assets folder
    	}
    	else
    	{ 
    		//By calling this method an empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
        	this.getReadableDatabase();
 
        	try 
        	{ 
    			copyDataBase();
    			System.out.println("Copy of DB success");
    		}
        	catch (IOException e)
        	{ 
        		throw new Error("Error copying database"); 
        	}
    	}
    }
    
    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    public boolean checkDataBase(){
 
    	File dbFile = myContext.getDatabasePath(DB_PATH);
    	return dbFile.exists();
    }
    
    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transferring bytestream.
     * */
    private void copyDataBase() throws IOException{
 
    	//Open your local db as the input stream
    	InputStream myInput = myContext.getAssets().open(DB_NAME);
 
    	// Path to the just created empty db
    	String outFileName = DB_PATH;
 
    	//Open the empty db as the output stream
    	OutputStream myOutput = new FileOutputStream(outFileName);
 
    	//transfer bytes from the inputfile to the outputfile
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0)
    	{
    		myOutput.write(buffer, 0, length);
    	}
 
    	//Close the streams
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
 
    }
    
    
    // When one thread is executing a synchronized method for an object, 
    // all other threads that invoke synchronized methods for the same object block (suspend execution) 
    // until the first thread is done with the object.
    @Override
	public synchronized void close() {
 
    	    if(myDataBase != null)
    		    myDataBase.close();
    	    super.close();
 
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

	// two dim string array that has imported the GrammErrors table from the DB
	public String[][] getGrammErrors() {
		
		String selectQuery = "SELECT  * FROM GrammErrors";
	    
	    // rawQuery executes the query and returns the result as a Cursor
	    Cursor myCursor = myDataBase.rawQuery(selectQuery, null);	
	    
	    int numberOfRowsInCursor = myCursor.getCount();
	    int numberOfColumnsInCursor = myCursor.getColumnCount();
	    
	    // two dim array reflecting the GrammErrors table
	    String[][] grammErrorArray = new String[numberOfColumnsInCursor][numberOfRowsInCursor];
	    
//	    //system.out.println("Cursor rows are: " + myCursor.getCount());
	    // Move to the first row
	    
	    if (myCursor.moveToFirst()) {
	    	int rowSeq = 0;
	    	do {
	        	// Store the key / value pairs in the array
	        	// Access the Cursor data by index that is in the same order
	        	// as used when creating the table
	        	
	    		for (int i = 0; i<numberOfColumnsInCursor; i++){
	    			grammErrorArray[i][rowSeq] = myCursor.getString(i);
//	    			//system.out.println("grammErrorArray["+i+"]["+rowSeq+"] = " + grammErrorArray[i][rowSeq]);
//				// GrammErrors.ID
//	        	grammErrorArray[0][rowSeq] = myCursor.getString(0);
//	        	// GrammErrors.TypeOfError
//	        	grammErrorArray[1][rowSeq] = myCursor.getString(1);
//	        	// GrammErrors.Tips
//	        	grammErrorArray[2][rowSeq] = myCursor.getString(2);
//	        	// GrammErrors.WordVariation1
//	        	grammErrorArray[3][rowSeq] = myCursor.getString(3);
//	        	// GrammErrors.WordVariation2
//	        	grammErrorArray[4][rowSeq] = myCursor.getString(4);
//	        	// GrammErrors.WordVariation3
//	        	grammErrorArray[5][rowSeq] = myCursor.getString(5);
//	        	// GrammErrors.WordVariation4
//	        	grammErrorArray[6][rowSeq] = myCursor.getString(6);
//	        	// GrammErrors.WordVariation5
//	        	grammErrorArray[7][rowSeq] = myCursor.getString(7);
//	        	// GrammErrors.WordVariation6
//	        	grammErrorArray[8][rowSeq] = myCursor.getString(8);
	    		}
	        	rowSeq++;
	        	
	        } while (myCursor.moveToNext()); // Move Cursor to the next row
	    }

//	    //system.out.println("grammErrorArray.length" + grammErrorArray[0].length);
	    // close the cursor or you will get the following error:
	    // 02-05 21:33:10.449: E/Cursor(3246): Finalizing a Cursor that has not been deactivated or closed. database = /data/data/com.joanthony.gnbw/databases/GNBW, table = null, query = SELECT  * FROM GrammErrors
	    myCursor.close();	    
	    // return contact list
	    return grammErrorArray;
	}
	
	public void updateNextPosition(int nextPosition, int bookID){
		String updateQuery = "UPDATE CurrentStatusOfBook SET BeginningLocOfCurrentLevelInFile = " + nextPosition + " WHERE _id = " + bookID;
		//system.out.println(updateQuery);
		myDataBase.execSQL(updateQuery);
	}

	public void addBook(String bookName, String path) {
		
		String query = "SELECT MAX(_id) AS max_id FROM CurrentStatusOfBook";
		Cursor cursor = myDataBase.rawQuery(query, null);

		int nextBookId = 0;     
		if (cursor.moveToFirst())
		{
		    do
		    {           
		    	nextBookId = cursor.getInt(0);                  
		    } while(cursor.moveToNext());           
		}
		nextBookId++;
		cursor.close();
		
		String updateQuery = "INSERT INTO CurrentStatusOfBook VALUES (" + nextBookId + ",'"+ bookName +"',"+ 0 +",'','"+ path +"')";
		
		myDataBase.execSQL(updateQuery);
	}

	public void updateOverallProgress(int currentArticlesScore, int countOfArticlesErrors, 
			int currentPronounsScore, int countOfPronounsErrors, 
			int currentDefinitionalScore, int countOfDefinitionalErrors,
			int currentPrepositionScore, int countOfPrepositionErrors,
			int currentTensesScore, int countOfTensesErrors) {
		
		// Increments OverallProgress table with current scores
		if (countOfArticlesErrors > 0){
			String updateQuery = "UPDATE OverallProgress SET NumberOfCorrectAnswers = NumberOfCorrectAnswers + " + currentArticlesScore + ", TotalNumberOfAnswers = TotalNumberOfAnswers + " + countOfArticlesErrors + " WHERE GrammarType = 'Articles'";
			System.out.println(updateQuery);
			myDataBase.execSQL(updateQuery);
		} 
		if (countOfPronounsErrors > 0){
			String updateQuery = "UPDATE OverallProgress SET NumberOfCorrectAnswers = NumberOfCorrectAnswers + " + currentPronounsScore + ", TotalNumberOfAnswers = TotalNumberOfAnswers + " + countOfPronounsErrors + " WHERE GrammarType = 'Pronouns'";
			System.out.println(updateQuery);
			myDataBase.execSQL(updateQuery);
		} 
		if (countOfDefinitionalErrors > 0){
			String updateQuery = "UPDATE OverallProgress SET NumberOfCorrectAnswers = NumberOfCorrectAnswers + " + currentDefinitionalScore + ", TotalNumberOfAnswers = TotalNumberOfAnswers + " + countOfDefinitionalErrors + " WHERE GrammarType = 'Definitional'";
			System.out.println(updateQuery);
			myDataBase.execSQL(updateQuery);
		}
		if (countOfPrepositionErrors > 0){
			String updateQuery = "UPDATE OverallProgress SET NumberOfCorrectAnswers = NumberOfCorrectAnswers + " + currentPrepositionScore + ", TotalNumberOfAnswers = TotalNumberOfAnswers + " + countOfPrepositionErrors + " WHERE GrammarType = 'Preposition'";
			System.out.println(updateQuery);
			myDataBase.execSQL(updateQuery);
		}
		if (countOfTensesErrors > 0){
			String updateQuery = "UPDATE OverallProgress SET NumberOfCorrectAnswers = NumberOfCorrectAnswers + " + currentTensesScore + ", TotalNumberOfAnswers = TotalNumberOfAnswers + " + countOfTensesErrors + " WHERE GrammarType = 'Tenses'";
			System.out.println(updateQuery);
			myDataBase.execSQL(updateQuery);
		}
	}

	public String[][] getScoreArray() {
		
		String selectQuery = "SELECT  * FROM OverallProgress";
	    
	    // Cursor provides read and write access for the 
	    // data returned from a database query
	    // rawQuery executes the query and returns the result as a Cursor
	    Cursor myCursor = myDataBase.rawQuery(selectQuery, null);	
	    
	    int numberOfRowsInCursor = myCursor.getCount();
	    int numberOfColumnsInCursor = myCursor.getColumnCount();
	    
	    // two dim array reflecting the OverallProgress table
	    String[][] scoreArray = new String[numberOfColumnsInCursor][numberOfRowsInCursor];

	    // Move to the first row	    
	    if (myCursor.moveToFirst()) {
	    	int rowSeq = 0;
	    	do {
	        	// Store the key / value pairs in the array
	        	// Access the Cursor data by index that is in the same order
	        	// as used when creating the table
	    		for (int i = 0; i<numberOfColumnsInCursor; i++){
	    			scoreArray[i][rowSeq] = myCursor.getString(i);
	    		}
	        	rowSeq++;
	        	
	        } while (myCursor.moveToNext()); // Move Cursor to the next row
	    }	

	    // close the cursor or you will get the following error:
	    // 02-05 21:33:10.449: E/Cursor(3246): Finalizing a Cursor that has not been deactivated or closed. database = /data/data/com.joanthony.gnbw/databases/GNBW, table = null, query = SELECT  * FROM GrammErrors
	    myCursor.close();
	    
	    // return SCORE list
	    return scoreArray;
	}

	public int getLatestBookID() {
		// Get book
		String qry = "SELECT _id FROM CurrentStatusOfBook WHERE LastOpenedOn <> '' ORDER BY LastOpenedOn DESC LIMIT 1";
 
	    // Cursor provides read and write access for the 
	    // data returned from a database query
	    // rawQuery executes the query and returns the result as a Cursor
	    Cursor myCursor = myDataBase.rawQuery(qry, null);	
		
	    try{
	    	myCursor.moveToFirst();
	    	int latestBookID = myCursor.getInt(0);
	    	System.out.println("latestBookID"+latestBookID);

	    	myCursor.close();
	    	
	    	return latestBookID;
	    } catch (Exception e){
	    	e.printStackTrace();
	    	myCursor.close();
	    	// -1 means no book
			return -1;
	    }

	}


	public void deleteBook(int BookID) {

		//BookID in parameter starts from 0, in DB it starts from 1
		BookID++;
		String deleteQry = "DELETE FROM CurrentStatusOfBook WHERE _id = " + BookID;
		
		myDataBase.execSQL(deleteQry);
		
		// run another query here to refresh all ids
		String refreshSequence = "UPDATE CurrentStatusOfBook SET _id = (_id - 1) WHERE _id > "+BookID;
		myDataBase.execSQL(refreshSequence);
//		myDataBase.close();
	}

	public void resetScore() {

		String resetQry = "UPDATE OverallProgress SET NumberOfCorrectAnswers = 0, TotalNumberOfAnswers = 0";
		
		myDataBase.execSQL(resetQry);
		
	}
	
	public String[] getLibraryIndex(){
		
		Cursor myCursor;

		String selectQuery = "SELECT * FROM CurrentStatusOfBook ORDER BY _id";
	    
		// Cursor provides read and write access for the 
	    // data returned from a database query
	    // rawQuery executes the query and returns the result as a Cursor
	    myCursor = myDataBase.rawQuery(selectQuery, null);
	    
	    // initiate array with a count of number of books
	    ArrayList<String> mArrayList = new ArrayList<String>(); //[myCursor.getCount()];
	    for(myCursor.moveToFirst(); !myCursor.isAfterLast(); myCursor.moveToNext()) {
	        // The Cursor is now set to the right position
	        mArrayList.add(myCursor.getString(1));
	    }
	    
	    //converting to String[]
	    String[] libIndex = new String[mArrayList.size()];
	    libIndex = mArrayList.toArray(libIndex);
	    
		return libIndex;
		
	}
	
	@SuppressLint("SimpleDateFormat")
	public void openBook(int bookID) {
		
		//bookID starts from 0 whereas in the DB the bookID starts from 1 
		bookID++;
		
		//initializing dates
		Date dt = new Date();
		// parsing in a format that is easy to sort in descending order from database in order to run continue and recent items
		SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyyMMddHHmmss");
		String parseDt = simpleDateformat.format(dt);
		
		//SYSTEM stores the lastOpenedOn information in DB
		String updateQuery = "UPDATE CurrentStatusOfBook SET LastOpenedOn = '" + parseDt + "' WHERE _id = " + bookID;
		System.out.println("updateQuery"+updateQuery);
		myDataBase.execSQL(updateQuery);

		//requery cursor to get latest lastPosition
		//SQLiteDatabase database = this.getReadableDatabase();
	    String selectQuery = "SELECT * FROM CurrentStatusOfBook ORDER BY _id";
	    Cursor myCursor = myDataBase.rawQuery(selectQuery, null);
	    
	    // bookid-1 because cursor starts from 0 and bookID starts from 1
 		myCursor.moveToPosition(bookID-1);

 		//_id, BookName, BeginningLocOfCurrentLevelInFile, FilePath, LastOpenedOn
 		//myCursor.getString(3) returns the filepath		
 		String bookPath = myCursor.getString(myCursor.getColumnIndex("FilePath"));
 		int lastPosition = myCursor.getInt(myCursor.getColumnIndex("BeginningLocOfCurrentLevelInFile"));
 		String bookName = myCursor.getString(myCursor.getColumnIndex("BookName"));
 		Intent openMainReader = new Intent(myContext, PageViewer.class);
 		
 		openMainReader.putExtra("BookPath", bookPath);
 		openMainReader.putExtra("BookID", bookID);
 		openMainReader.putExtra("BookName", bookName);
 		
 		// pass hardcoded number for testing
 		openMainReader.putExtra("LastPosition", lastPosition);
 		
 		myCursor.close();
 		
 		myContext.startActivity(openMainReader);
		
		
	}
	
}