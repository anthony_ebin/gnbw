package com.joanthony.gnbw;

import com.joanthony.gnbw.file_chooser.FileChooser;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Main Menu view will extend listActivity
 * @author Tony
 */
public class MainMenu extends ListActivity{
	
	// declare array for items to be pulled from the res->values->view_main_menu_list_data
	String[] listItems;

	/* 
	 * constructor to set the elements of the main menu
	 * @param bundle
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		// set listItems with the elements of the main menu
		listItems = this.getResources().getStringArray(R.array.mainMenuListItemsArray);
		
		//setup db to create tables and such; this prevents the following error:
		//05-23 12:08:57.504: I/Choreographer(11450): Skipped 61 frames!  The application may be doing too much work on its main thread.
		DataBaseHelper myDBHelper = new DataBaseHelper(this, true);
		myDBHelper.close();
		myDBHelper = null;
		// Bind resources Array to ListAdapter - i.e. choose which layout to use for the array items and where to put these 
		// formatted array items
		// having a variable layout for the list items allows us to change the layout in the future
		this.setListAdapter(new ArrayAdapter<String>(this, R.layout.menu_list_item,R.id.listItemsID, this.getResources().getStringArray(R.array.mainMenuListItemsArray)));

	}

	/* 
	 * Method that defines actions to take on click of any list item
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) 
	{
		super.onListItemClick(l, v, position, id);
		
		// switch function to make calls to the respective activities
		switch ((int)id) 
		{
	        case 0:
	        	//continue
	        	continueLastBook();
	        	break;
	        case 1:
	        	//lib
	        	Intent i = new Intent(this, LibraryIndex.class);
	            this.startActivity(i);
	        	break;
	        case 2:
	        	//score
	        	DataBaseHelper myDbHelper = new DataBaseHelper(this, true);
		        	
	        	String[][] scoreArray;
		        	
	        	//getting score from DB
	        	try {
	        		scoreArray= myDbHelper.getScoreArray();
				} catch (Exception e) {
					Toast.makeText(this, "There is nothing Here :)",Toast.LENGTH_SHORT).show();
	        		return;
				}
		        	
		        	
	        	// score
				Intent score = new Intent(this, ScoreActivity.class);
				// add scores
				for (int x = 0; x<=scoreArray[0].length-1 ; x++){ 
					//scoreArray[0][x] is _id
					//scoreArray[1][x] is GrammarType
					//scoreArray[2][x] is NumberOfCorrectAnswers
					//scoreArray[3][x] is TotalNumberOfAnswers
					ScoreActivity.addEntry(this, score, Integer.parseInt(scoreArray[0][x]), scoreArray[1][x], Integer.parseInt(scoreArray[2][x]), Integer.parseInt(scoreArray[3][x]));
				}
				
				myDbHelper.close();
				myDbHelper = null;
				this.startActivity(score);

				myDbHelper = null;
	        	break;
	        case 3:
	        	// import
				Intent file_chooser = new Intent(this, FileChooser.class);
				this.startActivity(file_chooser);
	        	break;
	        case 4:
	        	//settings
	        	Intent prefs = new Intent(this, Preferences.class);
	        	this.startActivity(prefs);
	        	break;
	        case 5:
	        	//about page
	        	Intent about = new Intent(this, About.class);
				this.startActivity(about);
	        default:
	        	break;
		}
	}

	private void continueLastBook() {
		
		DataBaseHelper libraryDbHelper = new DataBaseHelper(this,true);		
		
		int latestBookID = -1;
	    
		latestBookID = libraryDbHelper.getLatestBookID();
		
		// need to close db as it is being opened again in openbook
		libraryDbHelper.close();
		// destruct dbhelper
		libraryDbHelper = null;
		
		if (latestBookID > 0){
			//writable db
			DataBaseHelper dbHelper = new DataBaseHelper(this,false);	
			//-1 because when running open book the bookID is incremented by 1. This works for allowing
    		// 0 index bookid when using the library but when getting the latest book id the first one is 1
			dbHelper.openBook(latestBookID-1);
			dbHelper.close();
			dbHelper = null;
		} else {
			Toast.makeText(this, "There is nothing Here :)",Toast.LENGTH_SHORT).show();
			return;
		}
	}
}


