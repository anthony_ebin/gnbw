/**
 * 
 */
package com.joanthony.gnbw;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;

import com.joanthony.gnbw.R;

/**
 * @author Sweety
 *
 */
public class Preferences extends PreferenceActivity{

	/**
	 * 
	 */
	public Preferences() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see android.preference.PreferenceActivity#onCreate(android.os.Bundle)
	 */
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

		//error if user inputs less than 1 or more than 50 lines or font size less than 6 or more than 100
		validatePreference("MAX_LINES", 1, 50);
		validatePreference("FONT", 6, 100);

		
		
	}

	@SuppressWarnings("deprecation")
	private void validatePreference(final String pref, final int min, final int max) {
		findPreference(pref).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				Boolean rtnval = true;
				int val = Integer.parseInt(newValue.toString());
				if ((val<min)||(val>max)){
					rtnval = false;
					if (pref.equals("MAX_LINES")){
						Toast.makeText(Preferences.this, "Must be between 1 and 50", Toast.LENGTH_SHORT).show();
					} else if (pref.equals("FONT")){
						Toast.makeText(Preferences.this, "Must be between 6 and 100", Toast.LENGTH_SHORT).show();
					}
				}				
				return rtnval;
			}
		});
	}
}
