# Grammar Nazi Book Worm #

## Description

This is a game for people who love reading and want to improve their grammar at the same time. 
You may add any books of your choice (only text files are allowed as of version 1).
This game modifies the texts of the book by randomly including grammatical errors. 
You may correct the errors as you read and when you are ready hit submit; 
the game will show you where you were right, where you were wrong and give you a score.
As you go on, the scores are added to your overall progress which is accessible through the main menu. 
I hope this game helps you improve your grammar! 

### Screenshots from the play store:
#### More than 5000 installs and 4 star rated even with the terrible UI!
![Imgur](https://i.imgur.com/4NjtfeG.png)
![Imgur](https://i.imgur.com/JObwT17.png)
![Imgur](https://i.imgur.com/AT5mjOg.png)